const express = require('express')
const app = express()
const port = 80

app.set('view engine', 'pug')
app.set('views', '.')

app.get('/',  (req, res) => {
    res.render('index', { title: 'Hey', message: 'Hello there!', headers: req.headers })
  })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
