FROM node:alpine


RUN mkdir $HOME/k8s-sample-node

COPY ./ $HOME/

WORKDIR $HOME/

RUN yarn
CMD ["yarn", "exec", "node", "./main.js"]

#ENTRYPOINT "yarn", "exec", "node", "main.js"]
